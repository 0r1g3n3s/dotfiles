set number
set noerrorbells
set smartindent
set nowrap
set nobackup
set incsearch
set ignorecase
set encoding=utf-8
set showmatch
set termguicolors

call plug#begin('~/.local/share/nvim/plugged')

Plug 'ghifarit53/tokyonight-vim'
Plug 'scrooloose/nerdtree'
Plug 'yggdroot/indentline'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'ryanoasis/vim-devicons'
Plug 'dense-analysis/ale'


call plug#end()

"-------Tema------------"
set termguicolors

let g:tokyonight_style = 'night' " available: night, storm
let g:tokyonight_enable_italic = 1

colorscheme tokyonight

let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#left_sep = ' '
let g:airline#extensions#tabline#left_alt_sep = '|'
let g:airline#extensions#tabline#formatter = 'default'

"-----------------------"

"	Tecla Lider	"
let mapleader = ","
"-----------------------"

"	Atajos		"
"copy paste"
vmap <leader>y y
imap <leader>yt <esc><esc>P
nmap <leader>yt <esc><esc>P
"Undo"
imap <leader>d <esc><esc>u
nmap <leader>d u
"Delete a line lider + x"
imap <leader>x <esc><esc>:d<CR>
nmap <leader>x :d<CR>
"Guardado lider  + s
imap <leader>s <esc><esc>:w<CR>
nmap <leader>s :w<CR>
imap <leader>sa <esc><esc>:w
nmap <leader>sa :w
"Explorador de Archivos"
imap <leader>e <esc><esc>:NERDTree<CR>
nmap <leader>e :NERDTree<CR>
"Buscador"
nmap <leader>f :/
imap <leader>f <esc><esc>:/
"Cerrar"
imap <leader>c <esc><esc>:wq<CR>
imap <leader>ca <esc><esc>:q!<CR>
nmap <leader>c :wq<CR>
nmap <leader>ca :q!<CR>
"Select a line"
imap <leader>v <esc><esc>V
nmap <leader>v V
"Select per character"
imap <leader>vv <esc><esc>v
imap <leader>vv v
"exit mode or enter mode"
imap <leader><leader> <esc>
nmap <leader><leader> i
vmap <leader><leader> <esc>

"	Cosas Varias	"
let g:webdevicons_enable_nerdtree = 1
let NERDTreeShowHidden=1"

let g:ale_linters = {
	\   'python': ['flake8', 'pylint', 'bandit', 'mypy', 'pycodestyle'],
	\   'bash': ['bashate'],
	\}
let g:ale_fixers = {
	\   '*': ['remove_trailing_lines', 'trim_whitespace'],
	\   'python': ['black'],
	\}
let g:ale_lint_on_save = 1
let g:ale_fix_on_save = 1
let g:ale_sign_error = '💣'
let g:ale_sign_warning = '⚠️'
